#pragma once

#include <type_traits>

#include <boost/core/noncopyable.hpp>
#include <yas/binary_oarchive.hpp>
#include <yas/binary_iarchive.hpp>
#include <yas/file_streams.hpp>
#include <yas/types/std/pair.hpp>
#include <yas/types/std/bitset.hpp>
#include <yas/types/std/string.hpp>
#include <yas/types/std/wstring.hpp>
#include <yas/types/std/vector.hpp>
#include <yas/types/std/list.hpp>
#include <yas/types/std/forward_list.hpp>
#include <yas/types/std/array.hpp>
#include <yas/types/std/tuple.hpp>

#include <yas/detail/type_traits/type_traits.hpp>
#include <yas/detail/type_traits/serializer.hpp>

#include "src/main/cpp/detail/serialization.h"
#include "src/main/cpp/detail/serialization_stack.h"
#include "src/main/cpp/detail/serialization_armadillo.h"


namespace serializer {

class StringOutputStream : private boost::noncopyable {
 public:
  explicit StringOutputStream(std::string &underlying) : underlying_(underlying) {}

  size_t write(const void *ptr, const size_t size) {
    size_t cur = underlying_.size();
    underlying_.resize(cur + size);
    std::memcpy(&underlying_[0] + cur, reinterpret_cast<const char*>(ptr), size);
    return size;
  }

 private:
  std::string &underlying_;
};

class StringInputStream : private boost::noncopyable {
 public:
  explicit StringInputStream(const std::string &underlying) : underlying_(underlying) {}

  size_t read(void *ptr, const size_t size) {
    if (cur_ + size > underlying_.size()) {
      return 0;
    }
    std::memcpy(ptr, &underlying_[0] + cur_, size);
    cur_ += size;
    return size;
  }

 private:
  size_t cur_ = 0;
  const std::string &underlying_; // NOLINT
};

class Serializer {
 public:
  template <class T, class Ar>
  static constexpr bool CanSerializeTrivally() {
    using namespace yas::detail;
    // check that
    // 1. T does not have serialize functions
    // 2. T is trivially copyable
    return serialization_method<T, Ar>::value == yas::detail::ser_method::use_internal_serializer
        && std::is_trivially_copyable<T>::value;
  }

  // For trivially copyable types that do not have a serialize function, this function
  // serialize v by copying the bytes. For other types, fall back to the default yas implementation
  template <class T, class Ar>
  static void Serialize(const T& v, Ar &ar) {
    if constexpr (CanSerializeTrivally<T, Ar>()) {
      constexpr size_t size = sizeof(v);
      ar & reinterpret_cast<const uint8_t(&)[size]>(v);
    } else {
      ar & v;
    }
  }

 // For trivially copyable types that do not have a deserialize function, this function
  // deserialize v by copying the bytes. For other types, fall back to the default yas
  // implementation
  template <class T, class Ar>
  static void Deserialize(T& v, Ar &ar) {
    if constexpr (CanSerializeTrivally<T, Ar>()) {
      constexpr size_t size = sizeof(v);
      ar & reinterpret_cast<uint8_t(&)[size]>(v);
    } else {
      ar & v;
    }
  }

  template <class T>
  static void SerializeToString(const T &v, std::string &bytes) {
    StringOutputStream os(bytes);
    yas::binary_oarchive<StringOutputStream> oa(os);
    Serialize(v, oa);
  }

  template <class T>
  static void DeserializeFromString(T &v, const std::string &bytes) {
    StringInputStream is(bytes);
    yas::binary_iarchive<StringInputStream> ia(is);
    Deserialize(v, ia);
  }

  template <class T>
  static void SerializeToFile(const T &v, const std::string &filename) {
    yas::file_ostream os(filename.c_str(), yas::file_trunc);
    yas::binary_oarchive<yas::file_ostream> oa(os);
    Serialize(v, oa);
  }

  template <class T>
  static void DeserializeFromFile(T &v, const std::string &filename) {
    yas::file_istream is(filename.c_str());
    yas::binary_iarchive<yas::file_istream> ia(is);
    Deserialize(v, ia);
  }
};

} // namespace serializer
