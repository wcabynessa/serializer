#include <armadillo>
#include <fstream>
#include <iostream>
#include <map>
#include <stack>
#include <yas/std_streams.hpp>

#include "src/main/cpp/serializer.h"

using namespace serializer;
using namespace arma;

template <typename T>
void SaveToFile(const T &data, const std::string &filename) {
  std::ofstream ofs(filename,
                    std::ios::out | std::ios::trunc | std::ios::binary);
  yas::std_ostream yas_os(ofs);
  yas::binary_oarchive<yas::std_ostream> oa(yas_os);
  Serializer::Serialize(data, oa);
}

template <typename T>
void LoadFromFile(T &data, const std::string &filename) {
  std::ifstream ifs(filename, std::ios::in | std::ios::binary);
  yas::std_istream yas_is(ifs);
  yas::binary_iarchive<yas::std_istream> ia(yas_is);
  Serializer::Deserialize(data, ia);
}

int main() {
  // std::stack<char> x;
  // x.push('1');
  // x.push('2');
  // x.push('3');
  // SaveToFile(x, "test.out");

  // std::stack<char> y;
  // LoadFromFile(y, "test.out");
  // std::cout << y.size() << std::endl;
  // while (!y.empty()) {
  //   cout << y.top() << endl;
  //   y.pop();
  // }

  // Col<int> X(5, fill::randu);
  // int cnt = -2;
  // for (auto &u : X) {
  //   u = cnt++;
  // }
  // Col<int> Y;
  // SaveToFile(X, "test.out");
  // LoadFromFile(Y, "test.out");
  // assert(X == Y);
  // cout << X << endl;
  // cout << Y << endl;
  return 0;
}
