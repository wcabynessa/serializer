#pragma once

#include <armadillo>
#include <yas/detail/type_traits/serializer.hpp>
#include <yas/detail/type_traits/type_traits.hpp>

namespace yas {
namespace detail {

template <std::size_t F, class V, class Container>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    std::stack<V, Container>> {
  using StackType = std::stack<V, Container>;

  // can not traverse through stack, use pop() instead, pass by value to do so.
  template<class Ar>
  static Ar &save(Ar &ar, StackType stack) {
    ar.write_seq_size(stack.size());
    std::vector<V> vec;
    while (!stack.empty()) {
      vec.push_back(stack.top());
      stack.pop();
    }
    reverse(vec.begin(), vec.end());
    for (auto &u : vec) ar & u;
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, StackType &stack) {
    while (!stack.empty()) stack.pop();
    auto size = ar.read_seq_size();
    for (; size; --size) {
      V val{};
      ar & val;
      stack.push(std::move(val));
    }
    return ar;
  }
};

}  // namespace detail
}  // namespace yas
