#pragma once

#include <armadillo>
#include <yas/detail/type_traits/serializer.hpp>
#include <yas/detail/type_traits/type_traits.hpp>

namespace yas {
namespace detail {

template <std::size_t F, class V>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    arma::Mat<V>> {
  using MatType = arma::Mat<V>;

  template<class Ar>
  static Ar &save(Ar &ar, const MatType &mat) {
    ar.write_seq_size(mat.n_rows);
    ar.write_seq_size(mat.n_cols);
    for (const auto &u : mat) {
      ar & u;
    }
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, MatType &mat) {
    auto n_rows = ar.read_seq_size();
    auto n_cols = ar.read_seq_size();
    mat = MatType(n_rows, n_cols);
    for (auto &u : mat) {
      ar & u;
    }
    return ar;
  }
};

template <std::size_t F, class V>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    arma::Col<V>> {
  using ColType = arma::Col<V>;

  template<class Ar>
  static Ar &save(Ar &ar, const ColType &col) {
    ar.write_seq_size(col.n_rows);
    for (const auto &u : col) {
      ar & u;
    }
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, ColType &col) {
    auto n_rows = ar.read_seq_size();
    col = ColType(n_rows);
    for (auto &u : col) {
      ar & u;
    }
    return ar;
  }
};

template <std::size_t F, class V>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    arma::Row<V>> {
  using RowType = arma::Row<V>;

  template<class Ar>
  static Ar &save(Ar &ar, const RowType &row) {
    ar.write_seq_size(row.n_cols);
    for (const auto &u : row) {
      ar & u;
    }
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, RowType &row) {
    auto n_cols = ar.read_seq_size();
    row = RowType(n_cols);
    for (auto &u : row) {
      ar & u;
    }
    return ar;
  }
};

template <std::size_t F, class V>
struct serializer<
    type_prop::not_a_fundamental,
    ser_method::use_internal_serializer,
    F,
    arma::Cube<V>> {
  using CubeType = arma::Cube<V>;

  template<class Ar>
  static Ar &save(Ar &ar, const CubeType &cube) {
    ar.write_seq_size(cube.n_rows);
    ar.write_seq_size(cube.n_cols);
    ar.write_seq_size(cube.n_slices);
    for (const auto &u : cube) {
      ar & u;
    }
    return ar;
  }

  template<class Ar>
  static Ar &load(Ar &ar, CubeType &cube) {
    auto n_rows = ar.read_seq_size();
    auto n_cols = ar.read_seq_size();
    auto n_slices = ar.read_seq_size();
    cube = CubeType(n_rows, n_cols, n_slices);
    for (auto &u : cube) {
      ar & u;
    }
    return ar;
  }
};


} // namespace detail
}  // namespace yas
