#include "nami/daily/daily.h"
#include "nami/utils/op_util.h"
#include "modules/alpha_test_5dr/serialization_stack.h"

// put everything in an anonymous namespace to avoid naming conflicts
namespace {

class Alpha5DR : public nami::DailyAlpha {
 public:
  Alpha5DR() {
    set_stateless();
  }

  void Generate(int di) override {
    if (di + 1 < ndays_) return;

    auto returns_T = returns_->t();
    for (int ii = 0; ii < static_cast<int>(alpha_.size()); ii++) {
      if (valid(di, ii)) {
        auto row = returns_T.GetRow(ii);
        float ret = nami::OpUtil::Mean<float>(row.begin() + di - ndays_ + 1, row.begin() + di + 1);
        alpha_[ii] = -ret;
      }
    }
  }

 protected:
  void InitializeImpl() override {
    nami::DailyAlpha::InitializeImpl();

    ndays_ = conf<int>("ndays", 5);

    AddMeta("version", 20171129);
    AddMeta("ndays", ndays_);

    AddInputData("returns/returns", returns_);

    AddOutputState("stack", stack_);
  }

 private:
  const nami::Matrix<float> *returns_ = nullptr;
  std::stack<float> stack_;
  int ndays_;
};

}

extern "C" {
  nami::Module *MakeInstance() {
    return new Alpha5DR();
  }
}
